import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TheStyleComponent } from './the-style/the-style.component';
import { TheResumeComponent } from './the-resume/the-resume.component';


@NgModule({
  declarations: [
    AppComponent,
    TheStyleComponent,
    TheResumeComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
