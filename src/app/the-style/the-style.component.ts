import { Component, OnInit, Input } from '@angular/core';
// PrismJS for code hightlighting
import Prism from 'prismjs';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-the-style',
  templateUrl: './the-style.component.html',
  styleUrls: ['./the-style.component.css']
})
export class TheStyleComponent implements OnInit {

  @Input() code: string;

  //code in-style tag
  get codeInStyleTag(): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustHtml(`<style>${this.code}</style>`);
  }
  // get highlighted string with PrismJS
  // bypas.. bypass security and trust the given value to be safe style value (CSS).
  get highlightedCode(): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustHtml(Prism.highlight(this.code, Prism.languages.css));
  }
  constructor(private domSanitizer: DomSanitizer) { }
  ngOnInit() {
  }

}
