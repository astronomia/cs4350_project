import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheStyleComponent } from './the-style.component';

describe('TheStyleComponent', () => {
  let component: TheStyleComponent;
  let fixture: ComponentFixture<TheStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
