import { Component, OnInit } from '@angular/core';
import { THESTYLE, THEMARKDOWN, THEOFFER } from '../assets/STYLES';
// async, RXJS
import { from, of, concat, interval } from 'rxjs';
//rxjs operators 
import { delay, concatMap, tap, finalize } from 'rxjs/operators';


// the interval of progressivly rendering consecutive chars
const THAT_IS_SO_FAST = 0;
const INTERVAL = 15; 
const INTERVAL1 = 5;
const INTERVAL3 = 30;
const INTERVAL_LONG = 530;
const INTERVAL_LONG_2 = 100;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  private fullStyle = THESTYLE;
  private fullMarkdown = THEMARKDOWN;
  //Branch: delay interval
  private longerPauseCharacters = ['.', '!','?'];


  currentStyle = '';
  currentMarkdown = '';
  currentOffer = '';
  enableHtml = false;


  /* Pipe: takes as its arguments the functions you want to combine, and returns 
          a new function that, when executed, runs the composed functions in sequence.
  */ 

  // concatMap: concatALL, then map
  // delay: emit each Observable <token> by a given interval
  // tap: side effect => append token to our style&resume.
  // break whole styles/strings => tokens + delay => emit
  
  ngOnInit(): void {
    console.log('YES BOSS!');
    const styleSection0$ = from(this.fullStyle[0])
                          .pipe(     // of:  arguments => an observable sequence(s)
                            concatMap(char => of(char).pipe(
                              this.longerPauseCharacters.indexOf(char) > -1 ?
                              delay(INTERVAL_LONG) : delay(INTERVAL))), 
                              tap(char => this.currentStyle += char || ' ')
                          );
    const markdownSection$ = from(this.fullMarkdown)
                            .pipe(
                              concatMap(char => of(char).pipe(this.longerPauseCharacters.indexOf(char) > -1 ?
                              delay(INTERVAL_LONG) : delay(INTERVAL3))), 
                              tap(char => this.currentMarkdown += char || ' ')
                            );
    const styleSection1$ = from(this.fullStyle[1])
                          .pipe(
                            concatMap(char => of(char).pipe(this.longerPauseCharacters.indexOf(char) > -1 ?
                            delay(INTERVAL_LONG) : delay(INTERVAL1))), 
                            tap(char => this.currentStyle += char || ' '),
                            //enable HTML mapping of RESUME sections
                            //when processing index 1 of fullStyle
                            finalize(() => this.enableHtml = true)
                          );
    const styleSection2$ = from(this.fullStyle[2])
                          .pipe(
                            concatMap(char => of(char).pipe(this.longerPauseCharacters.indexOf(char) > -1 ?
                            delay(INTERVAL_LONG_2) : delay(INTERVAL1))), 
                            tap(char => this.currentStyle += char || ' ')
                          );
  
    //concat serveral Observables then subscribe to them.
    //execute until the one before is completed
    concat(
          styleSection0$, 
          markdownSection$, 
          styleSection1$, 
          styleSection2$
          ).subscribe();
  }

}
