
import { Component, OnInit,Input } from '@angular/core';
// DomSanitizer + SafeHTML
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
// MD to HTML
 import marked from 'marked';

@Component({
  selector: 'app-the-resume',
  templateUrl: './the-resume.component.html',
  styleUrls: ['./the-resume.component.css']
})

export class TheResumeComponent implements OnInit {
  
  @Input() markdown: string;
  // @Input() offer: string;
  @Input() enableHtml = false;
  // @Input() enableOffer = false;

  get result(): string | SafeHtml {
    //return HTML format when HTML "button" is enabled
    return this.enableHtml ? this.domSanitizer.bypassSecurityTrustHtml(marked(this.markdown)) : this.markdown;
  }

  constructor(private domSanitizer: DomSanitizer) { }

  ngOnInit() {
  }

}
