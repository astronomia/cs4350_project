import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheResumeComponent } from './the-resume.component';

describe('TheResumeComponent', () => {
  let component: TheResumeComponent;
  let fixture: ComponentFixture<TheResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
