export const THESTYLE = [
`
/*CSS...? Why not!
*/
.theStyle{
  font-size: 20px;
}
/*
* Hello! My name is Bo Chen.
* I am learning Angular.
* I like visualization.
* Talk is cheap. Show y' all the code.
*/
/* Big things often have small beginnings. 
*/
* {
  -webkit-transition: all 2s;
          transition: all 2s;
}

/* You will see.
.
.
.
Writing black on whiteboard is quite routine.
*/
html {
  background: rgb(12,13,14);
}
.token.comment      { color: white; }

/*  S....Sorry about you eyes!  Let's go. 
*/
.token.selector     { color: rgb(130,150,0);  }
.token.property     { color: rgb(180,130,0);  }
.token.punctuation  { color: yellow;          }
.token.function     { color: rgb(40,160,150); }

/* That color is not good. 
*/
.token.comment       { color: #857F6B; font-style: italic; }
/* We are still far away. 
*/
.theStyle {
  background:  rgb(66, 66, 66);
  height:  95vh;  width:   95vw;
  overflow:    auto;
  border:      2px solid #bbb;
  width:   50vw;
  box-shadow: -4px 4px 2px 0px rgba(0,0,0,0,0);
  margin: 1vh 0.5vw;
}

/* Résumé's ready to go. 
*/
.theStyle {
  height: 20vh;
  width:  95vw;
}

.theResume {
  background: rgb(68,68,68); color: #FFFFFF;
  width: 20vw; height: 20vh;
  overflow: auto;
  padding: .5em;  margin: 1vh 0.5vw;
  border: 1.5px solid #eee;
  width: 94vw;
  height: 70vh;
  font-size: 40px;
  font-size: 20px;
}
/* Let me write the resume. 
*/
  `
  
,

`
/* 
 * The formatting is not sufficiently HR-friendly.
 */
`
,
`
/* ALmost done. 
*/

html{
  perspective: 1000px;
}

.theResume h2{
  display: inline-block;
  border-bottom: 1px solid grey;
  margin: 1em 0 .5em;
}

.theResume ul, .theResume ol{
  list-style: none;
}
.theResume ul> li::before{
  content: '•';
  margin-right: .5em;
}
.theResume ol {
  counter-reset: section;
}
.theResume ol li::before {
  counter-increment: section;
  content: counters(section, ".") " ";
  margin-right: .5em;
}
.theResume blockquote {
  margin: 1em;
  padding: .5em;
  background: #aaa;
}

.theStyle {
  width: 40vw;
} 

.theResume{
 position: absolute;
 width: 40vw;
 left: 2px;
 left: 925px;
 top: 235px;
 top: 80px;
}
/* Wait, my boss.
   It tooks a while to render a good future.
*/

.theResume{
  height: 75vh;
  -webkit-transform: rotateY(-15deg) translateZ(-100px);
          transform: rotateY(-15deg) translateZ(-100px);
}

.theStyle {
  height: 90vh;
  -webkit-transform: rotateY(-15deg) translateZ(-100px);
          transform: rotateY(-15deg) translateZ(-100px);
  left: 10px;
} 

.theResume{
  left: 805px;
  height: 67.5vh;
  -webkit-transform: rotateY(10deg) translateZ(25px);
          transform: rotateY(10deg) translateZ(25px);
}

* {
  -webkit-transition: all 1.6s;
          transition: all 1.6s;
}
html{
  -webkit-transition: all 15s;
          transition: all 15s;
  background: rgb(255,255,255);
}
.theStyle{
  -webkit-transform: rotateY(-90deg) translateZ(-100px);
          transform-origin: center;
          transform: rotateY(-90deg) translateZ(-100px);
}
.theResume{
  -webkit-transform: rotateY(0deg) translateZ(0px);
          transform: rotateY(0deg) translateZ(0px);
  left: 624.5px;
  top: 125.25px;
  height: 62.4vh;
  box-shadow: -4px 4px 2px 0px rgba(0,0,0,0,0);
}
html{
  background: rgb(66,77,88);
}




`

,
`
.theStyle {

} 
.theResume{
  -webkit-transform-origin: right;
          transform-origin: right;
 
}
.theResume{
  -webkit-transform: rotateY(10deg) translateZ(25px);
          transform: rotateY(10deg) translateZ(25px);
}

}
.theStyle{
  -webkit-transform: rotateY(-15deg) translateZ(-100px);
          transform: rotateY(-15deg) translateZ(-100px);
}

.theStyle{
  -webkit-transform: rotateY(90deg) translateZ(-200px);
          transform: rotateY(90deg) translateZ(-200px);
}

.theResume{
  -webkit-transform: rotateY(90deg) translateZ(25px);
          transform: rotateY(90deg) translateZ(25px);
  box-shadow: -4px 4px 2px 0px rgba(0,0,0,0,0);
}


/* The boss said:
   .
   ..
   ...
   ....
   .....
 */
`
];


export const THEMARKDOWN = 
`
Bo Chen
-----------

* Senior majoring in computer science

Skills
-----------

* [Java,
* ... 
* Angular,
* ..., 
* +∞]
* Frontend development

Project & Paper
-----------

* [PL]  Su, Bo. "The Programming Language: Ruby" SUU, 2019
* [HCI] Cunningham, Daniel, et al. "Foveated Image Compression for Virtual Reality Streaming" SUU, 2019
* Pregressive Resume： alprazolamResume.web.app

Experience
-----------
1.Summer 2019 : Web design at Shanghai Cozy Sugery Co. LTD.
2.Summer 2016 : Web dev. at Wuxi Beien Sugery Co. LTD.

Find me at:
-----------

>* [GitHub]:     "https://github.com/cbrady3"

>* [SourceTree]: "https://bitbucket.org/astronomia"


`;

export const THEOFFER = 
`
  10


  9



  8



  7



  6




  5




  4




  3



  2




  1




  YOU ARE HIRED.
` 
;