#Progressive Resume
#CS4350 Project, Bo Chen, FALL 2019, SUU

#HTML5:
Essentially we have two non-static animating sections. 
Ng-template used for content switch from MARKDOWN to HTML format.

#CSS:
Styles, transition/animation, transformation.
Several chuncks of styles are imported as constants, progressively loaded with async functions.

#TyperScript:
TS has more robustness than JS.

#Framework: 
Angular + RxJS + PrismJS.
Mainly built with Angualr and RxJS, PrismJS is used to hightlight code segments in different colors.

#Node.JS:
New script is created to execute firebase init, deploy and ng serve in a fast fashion: "npm run dev"

#Firebase:
Hosted on firebase, URL: alprazolamresume.web.app

#Observable: 
To handle asyn data stream and emit chars as observables by a delay interval.

